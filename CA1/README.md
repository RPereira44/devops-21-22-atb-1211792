
## 1. Analysis, Design and Implementation



- 1 - Copy the code of the Tutorial React.js and Spring Data REST Application into a
new folder named CA1

````bash
mkdir CA1 (create a new folder) (file CA1 creation)
xcopy ".../tut-react-and-spring-data-rest" ".../CA1 /E (copy the files from tut-react-and-spring-data-rest to CA1 folder)
````

- 2 - Commit the changes (and push them)


````bash
  git pull
  git status
  git add .
  git commit -a -m "first commit"
  git push origin master
````
- 3 - We should use tags to mark the versions of the application. You should use a
pattern like: major.minor.revision (e.g., 1.1.0).
Tag the initial version as v1.1.0. Push it to the server.
````bash
git tag v1.1.0
git push origin v1.1.0
````
- 4 - Lets develop a new feature to add a new field to the application. In this case, lets
add a new field to record the years of the employee in the company (e.g., jobYears).
You should add support for the new field.
You should also add unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values). For the new field, only integer
values should be allowed.
You should debug the server and client parts of the solution.
When the new feature is completed (and tested) the code should be committed and
pushed and a new tag should be created (e.g, v1.2.0).

- - create a new field job years in the folder Employee.java and new methods set/get. 
- - add new data on the DataBaseLoader.java folder
- - add new data in the js folder apps.js.
- - create a folder EmployeeTest.java
- - add unit tests for all the fields, like null values, empty strings, negative values and only integers values.
- - go to browser (http://localhost:8080/) then F12 and check everything is working without errors.
 ````bash
 git status
 git add .
 git commit -a -m "First part CA1"
 git push origin master
 git tag v1.2.0
 git push v1.2.0
````
- 5 - At the end of the assignment mark your repository with the tag ca1-part1.
````bash
git tag ca1-part1
 git push origin ca1-part1
````
- 1 - You should use the master branch to "publish" the "stable" versions of the Tutorial React.js and Spring Data REST Application.
 
- 2 - You should develop new features in branches named after the feature. Create a branch named "email-field" to add a new email field to the application.
You should create a branch called email-field.
You should add support for the email field.
You should also add unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values).
You should debug the server and client parts of the solution.
When the new feature is completed (and tested) the code should be merged with the
master and a new tag should be created (e.g, v1.3.0).
 
````bash
  git branch emailField
  git checkout emailField
  ````
  - create a new field emailField in the folder Employee.java and new methods set/get.
  - go to browser (http://localhost:8080/) then F12 and check everything is working without errors.
  ````bash
  git status
  git checkout master
  git merge emailField
  git add .
  git commit -a -m "First part CA2"
  git push origin master
  git tag v1.3.0
  git push origin v1.3.0
````
- 3 - You should also create branches for fixing bugs (e.g., "fix-invalid-email").
Create a branch called fix-invalid-email. The server should only accept Employees
with a valid email (e.g., an email must have the "@" sign).
You should debug the server and client parts of the solution.
When the fix is completed (and tested) the code should be merged into master and a
new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)
 
````bash
git checkout -b fix-invalid-email
````
- add unit tests for the field emailField, like email field cannot be null and regex pattern where the emails must have the sign "@" .
- go to browser (http://localhost:8080/) then F12 and check everything is working.
````bash
 git status
 git checkout master
 git merge fix-invalid-email
 git add .
 git commit -a -m "update fix invalid email and tests"
 git push origin master
 git tag v1.3.1
 git push origin v1.3.1
````

- 4 - At the end of the assignment mark your repository with the tag ca1-part2.
````bash
git tag ca1-part2
git push origin ca1-part2
````


## 2. Analysis of an Alternative

There are several alternatives to git such as Mercurial, Helix Core and more.

For the purpose of this assigment the alternative chosen was the mercurial.



**Comparing both tools**

- *Implementation* – with its more numerous (and more specific) command structure, Git adoption can be a daunting task; especially for teams getting started on a DevOps transition or without developer leaders already familiar with Git work. Mercurial has specifically focused on a command structure that is simpler and ideally more intuitive for developers resulting in not only potentially faster adoption but anecdotally fewer mistakes during development due to errant commands.
- *Branching* – Mercurial creates a branch on change from the tip (Mercurial-speak for the newest branch in the central repository) which supports multiple concurrent changesets without children (heads) which can be merged into a branch or maintained in parallel to track changes side-by-side. In Git branches are intentional – i.e. not created just because something has changed – and very lightweight; working in branches is a part of many teams’ adoption of Git technology – so much so that there are popular terms for the practice referencing Git. Within a branch, Git uses a calculated SHA-1 checksum to distinguish that unique point in branch history.

- *History* – Git offers a few different options for “modifying” the history of a repository; commands such as rollback, cherry-pick, and rebase. Mutable history is an intentional choice designed to work in tandem with the multi-branching nature of Git repositories; for example, using rebase to collapse changes over time into a single logical point in the repository history. Mercurial does not permit such changes to the historical record by design.

- *Revision Tracking* – Mercurial tracks revisions sequentially by natural numbers (1, 2, 3, etc) making it more intuitive for developers to understand the order of operations of changes over time. Git computes a SHA-1 hash at the time of a commit that is a combination of the metadata and the hash of the root object; this method is out of the scope of this article but well worth a read for any teams adopting Git. The resulting SHA means that two (or more) developers viewing a commit in the stream of revisions can be guaranteed to be viewing the same status, which can be especially important for highly distributed teams.

- *Rollback* – As a part of managing the lifecycle of a repository, Git includes a revert command which creates a new commit in a state without any of the changes included as part of the specified historical commit. This action does not modify the history as the original, reverted commit is kept in its place, simply that the effects of that commit are undone. Mercurial offers similar functionality via its backout and revert commands, used in conjunction with a merge.



- **Despite the literature agrees the mercurial is more easy to use and simpler , in my small experience ,I thought git was more organized, logical and structured to work with. Probably due to the fact, that the first tool and the tool we use in da daily use is  git.**
## 3. Implementation of the Alternative

*Present the implementation of this class assignment using the git alternative*

- 1 - Copy the code of the Tutorial React.js and Spring Data REST Application into a
new folder named CA1
````bash
mkdir CA1 (create a new folder)
xcopy ".../tut-react-and-spring-data-rest" ".../CA1 /E (copy the files from tut-react-and-spring-data-rest to CA1 folder)
````
- 2 - Commit the changes (and push them)
 
````bash
 hg pull
 hg status
 hg add .
 hg commit  -m "first commit"
 hg push default
````
- 3 - We should use tags to mark the versions of the application. You should use a
pattern like: major.minor.revision (e.g., 1.1.0).
Tag the initial version as v1.1.0. Push it to the server.
````bash
 hg tag v1.1.0
 hg commit -m "v1.1.0"
 hg push  v1.1.0
````
- 4 - Lets develop a new feature to add a new field to the application. In this case, lets
add a new field to record the years of the employee in the company (e.g., jobYears).
You should add support for the new field.
You should also add unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values). For the new field, only integer
values should be allowed.
You should debug the server and client parts of the solution.
When the new feature is completed (and tested) the code should be committed and
pushed and a new tag should be created (e.g, v1.2.0).

- create a new field job years in the folder Employee.java and new methods set/get.
- add new data on the DataBaseLoader.java folder
- add new data in the js folder apps.js.
- create a folder EmployeeTest.java
- add unit tests for all the fields, like null values, empty strings, negative values and only integers values.
- go to browser (http://localhost:8080/) then F12 and check everything is working without errors.
````bash
 hg status
 hg add .
 hg commit -m "First part CA1"
 hg push
 hg tag v1.2.0
 hg commit -m "v1.2.0"
 hg push v1.2.0
````
- 5 - At the end of the assignment mark your repository with the tag ca1-part1.

````bash
 hg tag ca1-part1
 hg commit -m "v1.1.0"
 hg push origin ca1-part1
````

- 1 - You should use the master branch to "publish" the "stable" versions of the Tutorial React.js and Spring Data REST Application.

- 2 - You should develop new features in branches named after the feature. Create a branch named "email-field" to add a new email field to the application.
You should create a branch called email-field.
You should add support for the email field.
You should also add unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values).
You should debug the server and client parts of the solution.
When the new feature is completed (and tested) the code should be merged with the
master and a new tag should be created (e.g, v1.3.0).
````basg
 hg branch email-field
 hg add .
 hg commit -m "email-field"
 hg push --new-branch
 hg push
 hg identify -b
 hg update emailField
 ````
- create a new field emailField in the folder Employee.java and new methods set/get.
- got to browser (http://localhost:8080/) then F12 and check everything is working without errors.
````bash
 hg status
 hg update default
 hg merge email-field
 hg add .
 hg commit -m "First part CA2"
 hg push 
 hg tag v1.3.0
 hg commit -m "v1.3.0"
 hg push 
````
- 3 - You should also create branches for fixing bugs (e.g., "fix-invalid-email").
Create a branch called fix-invalid-email. The server should only accept Employees
with a valid email (e.g., an email must have the "@" sign).
You should debug the server and client parts of the solution.
When the fix is completed (and tested) the code should be merged into master and a
new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)

````bash
 hg branch fix-invalid-email
 hg add .
 hg commit -m "fix-invalid-email"
 hg push --new-branch
 hg push
 hg identify -b
 hg update fix-invalid-email
````
- add unit tests for the field emailField, like email field cannot be null and regex pattern where the emails must have the sign "@" .
- got to browser (http://localhost:8080/) then F12 and check everything is working without errors.

````bash
 hg status
 hg update default
 hg merge fix-invalid-email
 hg add .
 hg commit -m "First part CA2"
 hg push
 hg tag v1.3.1
 hg commit -m "v1.3.1"
 hg push 
````

- 4 - At the end of the assignment mark your repository with the tag ca1-part2.
````bash
 hg tag ca1-part2
 hg commit -m "ca1-part2"
 hg push origin ca1-part1
````
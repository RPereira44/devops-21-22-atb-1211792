package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void emptyStringFirstName() {

        //Arrange
        String firstName = "";
        String lastName = "Baggins";
        String description = "Ring bearer";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void emptyStringWithSpaceFirstName() {

        //Arrange
        String firstName = "  ";
        String lastName = "Baggins";
        String description = "Ring bearer";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";

        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void nullFirstName() {

        //Arrange
        String firstName = null;
        String lastName = "Baggins";
        String description = "Ring bearer";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }
    @Test
    void emptyStringLastName() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "";
        String description = "Ring bearer";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";

        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }
    @Test
    void emptyStringWithSpaceLastName() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "   ";
        String description = "Ring bearer";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";

        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void nullLastName() {

        //Arrange
        String firstName = "Frodo";
        String lastName = null;
        String description = "Ring bearer";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";

        //Act & assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void emptyStringDescription() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";

        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }
    @Test
    void emptyStringDescriptionWithSpace() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "";
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void nullDescription() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = null;
        String jobTitle = "devOps";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void emptyStringJobTitle() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = "";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }
    @Test
    void emptyStringWithSpaceJobTitle() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = "";
        int jobYears = 2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void nullJobTitle() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = null;
        int jobYears = 0;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void negativeJobYears() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = "devOps";
        int jobYears = -2;
        String emailField = "frodo@lotr.com";


        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

    @Test
    void createEmployeeWithZeroJobYears() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = "devOps";
        int jobYears = 0;
        String emailField = "frodo@lotr.com";



        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears,emailField);
        Employee employee1 = new Employee(firstName,lastName,description,jobTitle,jobYears,emailField);


        assertEquals(employee,employee1);
    }

    @Test
    void nullEmail() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = "hero";
        int jobYears = 0;
        String emailField = null;


        //Act & assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }
    @Test
    void invalidEmail() {

        //Arrange
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Ring Bearer";
        String jobTitle = "hero";
        int jobYears = 5;
        String emailField = "frodolotr.com";


        //Act & assert
        assertThrows(IllegalArgumentException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears,emailField)
        );
    }

}
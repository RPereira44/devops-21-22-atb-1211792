# Class Assignment 2 Report

## 1. Analysis, Design and Implementation

### Part 1

- First I created a new folder with name CA2 and then a new one with the name Part01.
After that I downloaded and commit (deleted .git file in the folder) to my repository the example application available at
  https://bitbucket.org/luisnogueira/gradle_basic_demo/.


- *After I read the instructions available in the readme.md file and experiment with the application.
Where I could open several terminals  and write messages. I started to make/answer the tasks (point 3,4,5,6,7)* 


- Point 3 - Add a new task to execute the server.


```groovy

task runServer(type:JavaExec, dependsOn: classes){

group = "DevOps"

description = "Launches a chat client that connects to a server on localhost:59001 "

   classpath = sourceSets.main.runtimeClasspath

  mainClass = 'basic_demo.ChatServerApp'

 args '59001'

}
```

- Point 4 - Add a simple unit test and update the gradle script so that it is able to execute the
  test.

````groovy
test {

    // for all tests
    useJUnit()


    minHeapSize = "128m"
    maxHeapSize = "512m"

    filter {

        //include all tests from package
        includeTestsMatching "basic_demo.*"
    }

    testLogging {
        afterSuite { desc, result ->
            if (!desc.parent) { // will match the outermost suite
                def output = "Results: ${result.resultType} (${result.testCount} tests, ${result.successfulTestCount} passed, ${result.failedTestCount} failed, ${result.skippedTestCount} skipped)"
                def startItem = '|  ', endItem = '  |'
                def repeatLength = startItem.length() + output.length() + endItem.length()
                println('\n' + ('-' * repeatLength) + '\n' + startItem + output + endItem + '\n' + ('-' * repeatLength))
            }
        }
    }

}
````

- Point 5 - Add a new task of type Copy to be used to make a backup of the sources of the application. 

````groovy
task copy(type: Copy){

from 'src'

into 'backup'

}
`````

- Point 6 - Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application. 

````groovy
task zip(type: Zip){

from 'src'

archiveName  'backup.zip'

destinationDir(file('backupZip'))

}
````

- Point 7 - At the end of the part 1 of this assignment mark your repository with the tag
ca2-part1.

````bash
git tag ca2-part1
git push origin ca2-part1
````

### Part2

#### The goal of Part 2 of this assignment is to convert the basic version (i.e., "basic" folder) of the Tutorial application to Gradle (instead of Maven)

- Point 1 In your repository create a new branch called tut-basic-gradle. You should use this branch for this part of the
  assignment (do not forget to checkout this branch).

````bash
git checkout -b tut-basic-gradle
````
- *The following points 2 ,3, 4, 5, 6, 7 were realized by following the steps described by the pdf provided by the teacher.*

- Point 8 - Add one of the following lines to the plugins block in build.gradle (select the line according to your version of
  java: 8 or 11):
````groovy
plugins {
  id 'org.springframework.boot' version '2.6.6'
  id 'io.spring.dependency-management' version '1.0.11.RELEASE'
  id 'java'
  id "org.siouan.frontend-jdk11" version "6.0.0"
}
````

- Point 9 - Add also the following code in build.gradle to configure the previous plugin:

````groovy
plugins {
  id 'org.springframework.boot' version '2.6.6'
  id 'io.spring.dependency-management' version '1.0.11.RELEASE'
  id 'java'
  //Point 8.
  id "org.siouan.frontend-jdk11" version "6.0.0"
  }

group = 'com.greglturnquist'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

repositories {
mavenCentral()
}

dependencies {
implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
implementation 'org.springframework.boot:spring-boot-starter-data-rest'
implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
runtimeOnly 'com.h2database:h2'
testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

tasks.named('test') {
useJUnitPlatform()
}
//Point 9.
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}
````

- Point 10 - Update the scripts section/object in package.json to configure the execution of webpack:


````json
 {
  "name": "spring-data-rest-and-reactjs",
  "version": "0.1.0",
  "description": "Demo of ReactJS + Spring Data REST",
  "repository": {
  "type": "git",
  "url": "git@github.com:spring-guides/tut-react-and-spring-data-rest.git"
  },
  "keywords": [
  "rest",
  "hateoas",
  "spring",
  "data",
  "react"
  ],
  "author": "Greg L. Turnquist",
  "license": "Apache-2.0",
  "bugs": {
  "url": "https://github.com/spring-guides/tut-react-and-spring-data-rest/issues"
  },
  "homepage": "https://github.com/spring-guides/tut-react-and-spring-data-rest",
  "dependencies": {
  "react": "^16.5.2",
  "react-dom": "^16.5.2",
  "rest": "^1.3.1"
  },
  "scripts": {
  "webpack": "webpack",
  "build": "npm run webpack",
  "check": "echo Checking frontend",
  "clean": "echo Cleaning frontend",
  "lint": "echo Linting frontend",
  "test": "echo Testing frontend"
  },
  "devDependencies": {
  "@babel/core": "^7.1.0",
  "@babel/preset-env": "^7.1.0",
  "@babel/preset-react": "^7.0.0",
  "babel-loader": "^8.0.2",
  "webpack": "^4.19.1",
  "webpack-cli": "^3.1.0"
  }
  }
````


- *Through points 11 and 12 it was possible to verify that the tasks related to the frontend and the code were working as well as the app.*


- Point 13 - Add a task to gradle to copy the generated jar to a folder named "dist" located a the project root folder level

````groovy
task copy(type: Copy) {
    from fileTree("build/libs").matching {
        include "*.jar"
    }
    into "dist"
}
````

- Point 14 - Add a task to gradle to delete all the files generated by webpack (usually located at
  src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.


````groovy
task deleteBuilt(type: Delete){
delete "src/main/resources/static/built"

}

clean.configure{
dependsOn deleteBuilt

}
````

- Point 15 - Experiment all the developed features and, when you feel confident that they all work commit your code and merge
  with the master branch.


````bash
git checkout master
git merge  tut-basic-gradle
````

- Point 17 - At the end of part 2 of this assignment mark your repository with the tag ca2-part2.

````bash
git tag ca2-part2
git push origin ca2-part2
````

## 2. Analysis of an Alternative

As alternative to the tool gradle I used the maven tool.


| Basis  | Gradle  |  Maven |  
|---|---|---|
| Based on  | Gradle is based on developing domain-specific language projects.   | Maven is based on developing pure Java language-based software.  |   
| Configuration  | It uses a Groovy-based Domain-specific language(DSL) for creating project structure.  | It uses Extensible Markup Language(XML) for creating project structure.  |   
| Focuses on  | Developing applications by adding new features to them.  | Developing applications in a given time limit.   |   
| Performance  | It performs better than maven as it optimized for tracking only current running task.  | It does not create local temporary files during software creation, and is hence – slower.  |   
| Java Compilation  | It avoids compilation.  | It is necessary to compile.   |   
| Usability  | It is a new tool, which requires users to spend a lot of time to get used to it.  | This tool is a known tool for many users and is easily available.  |   
| Customization  | This tool is highly customizable as it supports a variety of IDE’s.  | This tool serves a limited amount of developers and is not that customizable.  |   
| Languages supported | It supports software development in Java, C, C++, and Groovy.  | It supports software development in Java, Scala, C#, and Ruby.   | 



- In my small experience with both tools I realized gradle is more intuitive and, possible, more easy to use due to syntax and logical language. 
- It is simple even for a rookie like me, to understand what the tasks (gradle) are doing and when they are doing.
- Working with the maven was a little more tricky because I had to understand the plugins and how they work , in each part of the cycle.
- Nevertheless, I have the notion that I only started to see/understand a small part of both tools. The fact tha both are new to me, could help/influence how I see and respond to the tools, because I do not have any experience with none of them.

## 3. Implementation of the Alternative
- Point 1 - In your repository create a new branch called tut-basic-gradle. You should use this branch for this part of the
  assignment (do not forget to checkout this branch).

````bash
git checkout -b ca2-part2-alternative
````

*In order to execute the points/ tasks asked I had to open the file pom.xml(react-and-spring-data-rest-basic), and proceed to the alterations on the file.*

- Point 13 - Add a task to gradle to copy the generated jar to a folder named "dist" located a project root folder level

````javascript
<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.0.2</version>
				<executions>
					<execution>
						<id>copy-resource-one</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>dist</outputDirectory>
							<resources>
								<resource>
									<directory>target</directory>
									<includes>
										<include>*.jar</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
````

- Point  14 - Add a task to gradle to delete all the files generated by webpack (usually located at
  src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.

````javascript
          <plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.1.0</version>
				<executions>
					<execution>
						<id>auto-clean</id>
						<phase>pre-clean</phase>
						<goals>
							<goal>clean</goal>
						</goals>
						<configuration>
							<filesets>
								<fileset>
									<directory>src/main/resources/static/built/</directory>
								</fileset>
							</filesets>
						</configuration>
					</execution>
				</executions>
			</plugin>
````

- Point 15 - Experiment all the developed features and, when you feel confident that they all work commit your code and merge
  with the master branch.

````bash
 git checkout master
 git merge  ca2-part2-alternative
````

- Point 17 - At the end of part 2 of this assignment mark your repository with the tag ca2-part2.

````bash
 git tag ca2-part2
 git push origin ca2-part2
````
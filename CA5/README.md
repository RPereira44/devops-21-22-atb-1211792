# Class Assignment 4 Report

## 1. Analysis, Design and Implementation

#### About Jenkins:

- Jenkins is a powerful application that allows continuous integration and continuous delivery of projects,
regardless of the platform we are working on. It is a free source that can handle any kind of build or continuous integration. 
Jenkins can be integrated with a number of testing and deployment technologies.

- Adoption: Jenkins is widespread, with more than 147,000 active installations and over 1 million users around the world.
Plugins: Jenkins is interconnected with well over 1,000 plugins that allow it to integrate with most of the development, testing and deployment tools.
It is evident from the above points that Jenkins has a very high demand globally.


### CA5-Part1

On this part the goal for this assigment was to practice with Jenkins using the "gradle
basic demo" project, present at https://bitbucket.org/RPereira44/devops-21-22-atb-1211792/CA2/Part01/gradle_basic_demo/.
 
In order to be able to complete this job I had to open an account and follow the steps mentioned by teachers class "Continuous Integration and Delivery from 23/05/2022".


#### *Version Pipeline CA5-part01*

- In order to make a new pipeline in Jenkins I had to go to the Jenkins dashboard and press in the new item:

![img_11.png](img_11.png)


- Enter a name and select the pipeline option:


![img_12.png](img_12.png)

- Select the configure option:


![img_13.png](img_13.png)


#### In this version  it was used a script directly in the jenkins dashboard.

- Select the pipeline script:

![img_14.png](img_14.png)

- After that introduce the script on the respective field:

![img_15.png](img_15.png)

- Script:

````Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/RPereira44/devops-21-22-atb-1211792'


            }
        }
        stage('Assemble') 
        {
            steps {
                echo 'Assembling...'
                dir ("CA2/Part01/gradle_basic_demo") {
                    script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew assemble'}
                    else
                        bat './gradlew assemble'
                }


                }

            }
        }
        stage('Test') {
            steps {
                echo 'testing...'
                dir ("CA2/Part01/gradle_basic_demo"){
                     script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    else
                    {
                        bat './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                }

                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ("CA2/Part01/gradle_basic_demo"){
                archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
}
````

- Press save, and select the "Build Now" option.

- Result:

![img_4.png](img_4.png)



#### *Version Pipeline CA5-part01-SCM*


#### In this version  it was used a Jenkinsfile, that was sent to my repository.

- We to choose the option configure on the dashboard.

- Change the pipeline definition to:

![img_16.png](img_16.png)

- Paste the repository:

![img_1.png](img_1.png)

- And the script path inside the repository:

![img_6.png](img_6.png)

- Press save.

- In the dashboard choose the option "Build Now" and wait:

![img_7.png](img_7.png)



### CA5 Part02


In order to complete the assigment for part02 I had to follow the same steps from the previous work.

On this part the goal for this assigment was to practice with Jenkins using the "gradle basic demo" project,
present at https://bitbucket.org/RPereira44/devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic.

In this assignment was asked to use the project from the CA2 Part 02, and to add plus, 
the steps, "Javadoc", "publishHTML" and "Publish Image".


- The new script:

````Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/RPereira44/devops-21-22-atb-1211792'


            }
        }
        stage('Assemble') 
        {
            steps {
                echo 'Assembling...'
                dir ("CA2/Part02/react-and-spring-data-rest-basic") {
                    script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew assemble'}
                    else
                        bat './gradlew assemble'
                }


                }

            }
        }
        stage('Test') {
            steps {
                echo 'testing...'
                dir ("CA2/Part02/react-and-spring-data-rest-basic"){
                     script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    else
                    {
                        bat './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                }

                }
            }
        }
         stage('JavaDoc') {
                    steps {
                        echo 'documenting...'
                        dir ("CA2/Part02/react-and-spring-data-rest-basic"){
                               script {
                                if (isUnix()){
                                sh './gradlew javadoc'

                                javadoc javadocDir: 'build/docs/javadoc', keepAll: false}
                                 else{
                                 bat './gradlew javadoc'
                                javadoc javadocDir: 'build/docs/javadoc', keepAll: false
                                }
                        }
                        }
                    }
                }
         stage('HTML') {
                    steps {
                        echo 'documenting...'
                        dir ("CA2/Part02/react-and-spring-data-rest-basic"){


                                 publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/docs/javadoc', reportFiles: 'index-all.html', reportName: 'HTML Report', reportTitles: 'Docs Loadtest Dashboard'])
                            
                        }
                    }
                }        
                
                

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ("CA2/Part02/react-and-spring-data-rest-basic"){
                archiveArtifacts 'build/libs/*'
                }
            }
        }
         stage ('Docker Image') {
                  steps {
                    
                    script {
                           docker.withRegistry( '', 'docker' ){
                        def image =  docker.build("rpereira44/versionjenkins","ca2/part02/react-and-spring-data-rest-basic")
                            image.push()     
                           }
                    }
                  }
                }
    }
}
````

- Script directly into jenkins:

![img_10.png](img_10.png)

- From Jenkinsfile in the repository:

![img_17.png](img_17.png)

- Docker hub:

![img_21.png](img_21.png)


## 2. Analysis of an Alternative

Has an alternative I have chosen bitbucket pipelines.

Bitbucket Pipelines is an integrated CI/CD service built into Bitbucket. 
It allows  automatically build, test, and  deploy code based on a configuration file in the repository.


#### Bitbucket vs Jenkins: What are the differences?

Developers describe Bitbucket as "One place to plan projects, collaborate on code, test and deploy, all with free private repositories". Bitbucket gives teams one place to plan projects, collaborate on code, test and deploy, all with free private Git repositories. Teams choose Bitbucket because it has a superior Jira integration, built-in CI/CD, & is free for up to 5 users. On the other hand, Jenkins is detailed as "An extendable open source continuous integration server". In a nutshell Jenkins CI is the leading open-source continuous integration server. Built with Java, it provides over 300 plugins to support building and testing virtually any project.

Bitbucket can be classified as a tool in the "Code Collaboration & Version Control" category, while Jenkins is grouped under "Continuous Integration".

- Some features offered by Bitbucket are:

  Unlimited private repositories, charged per user
  Best-in-class Jira integration
  Built-in CI/CD
  On the other hand, Jenkins provides the following key features:

  Easy installation

  Easy configuration

  Change set support

  "Free private repos", "Simple setup" and "Nice ui and tools" are the key factors why developers consider Bitbucket; whereas "Hosted internally", "Free open source" and "Great to build, deploy or launch anything async" are the primary reasons why Jenkins is favored.

Jenkins is an open source tool with 13.2K GitHub stars and 5.43K GitHub forks.
According to the StackShare community, Jenkins has a broader approval, being mentioned in 1753 company stacks & 1479 developers stacks; compared to Bitbucket, 
which is listed in 1735 company stacks and 1449 developer stacks.


## 3. Implementation of the Alternative

In order to use bitbucket pipeline we need to choose the option pipelines:

![img_20.png](img_20.png)

- Create the script.

Despite some resemblances with the script used in jenkins, the commands and orders have differences, 
and we needed to define variables,as usernames and passwords from other services used by the script.
And it is necessary to pass the cache/file to the next step.

- The bitbucket-pipelines.yml file:

````yml
image: gradle:7.4.1-jdk11

pipelines:
  default:
    - step:
        name: Checkout
        script:
          - git clone https://bitbucket.org/RPereira44/devops-21-22-atb-1211792
        artifacts:
          - devops-21-22-atb-1211792/**
    - step:
        name: Assemble
        caches:
          - gradle
        script:
          - cd devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic
          - bash ./gradlew assemble
        artifacts:
          - devops-21-22-atb-1211792/**
    - step:
        name: Test
        script:
          - cd devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic
          - bash ./gradlew test
        artifacts:
          - devops-21-22-atb-1211792/**
    - step:
        name: Javadoc
        script:
          - cd devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic
          - bash ./gradlew javadoc
        artifacts:
          - devops-21-22-atb-1211792/**
    - step:
        name: Publish
        script:
          - cd devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic
          - bash ./gradlew publish
        artifacts:
          - devops-21-22-atb-1211792/**
    - step:
        name: Archive
        script:
          - pipe: atlassian/bitbucket-upload-file:0.3.2
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USERNAME
              BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
              FILENAME: devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic/build/libs/*

    - step:
        name: Docker Image Push
        script:
          - cd devops-21-22-atb-1211792/CA2/Part02/react-and-spring-data-rest-basic
          - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
          - docker build -t $DOCKER_HUB_REPOSITORY:BB_$BITBUCKET_BUILD_NUMBER .
          - docker push $DOCKER_HUB_REPOSITORY:BB_$BITBUCKET_BUILD_NUMBER
        services:
          - docker
````

- Pipeline result:

![img_18.png](img_18.png)

- Archive files:

![img_19.png](img_19.png)

- Docker repository:

![img_22.png](img_22.png)



## Conclusion

The scripts from both tools have similarities, in the way they are build.
But, despite it , in my opinion Jenkins is a more complete tool, int terms of organization,
and letting put all the important information on the dashboard, and the plugins are huge plus because they permit high customization. 




### *As a personal work, I decided to explore another tool as Buddy.*



- Comparison between Jenkins and Buddy:

| Category     |                     Jenkins                     |               Buddy               |
|:-------------|:-----------------------------------------------:|:---------------------------------:|
| Installation |     Java application requires installation.     |      No installation needed.      |
| Maintenance  | Requires regular patching, tuning and cleaning. |      No maintenance needed.       |
| Integrations |              Open source plugins.               | Pre-made and tested integrations. |
| Pipelines    |             Scripted and UI driven.             |      Scripted and UI driven.      |



- Pipeline:

![img_23.png](img_23.png)

- Configuration / build configurations (example):

![img_24.png](img_24.png)

- Pipeline result:

![img_29.png](img_29.png)

![img_28.png](img_28.png)

- .jar file:

![img_25.png](img_25.png)

- Javadoc :

![img_26.png](img_26.png)

- HTML:

![img_27.png](img_27.png)

- YAML config:

````YAML
- pipeline: "CA5-Par02-Alt"
  on: "EVENT"
  events:
  - type: "PUSH"
    refs:
    - "refs/heads/master"
      priority: "NORMAL"
      fail_on_prepare_env_warning: true
      actions:
  - action: "Execute: gradle assemble"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-atb-1211792"
    docker_image_name: "library/gradle"
    docker_image_tag: "7.4.2"
    execute_commands:
    - "cd \"CA2/Part02/react-and-spring-data-rest-basic\""
    - ""
    - "sh ./gradlew assemble"
      volume_mappings:
    - "/:/buddy/devops-21-22-atb-1211792"
      cache_base_image: true
      shell: "BASH"
  - action: "Execute: gradle test"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-atb-1211792"
    docker_image_name: "library/gradle"
    docker_image_tag: "7.4.2"
    execute_commands:
    - "cd \"CA2/Part02/react-and-spring-data-rest-basic\""
    - "sh ./gradlew test"
      volume_mappings:
    - "/:/buddy/devops-21-22-atb-1211792"
      cache_base_image: true
      shell: "BASH"
  - action: "Build Docker image"
    type: "DOCKERFILE"
    dockerfile_path: "CA2/Part02/react-and-spring-data-rest-basic/Dockerfile"
    context_path: "CA2/Part02/react-and-spring-data-rest-basic"
    target_platform: "linux/amd64"
  - action: "Push Docker image"
    type: "DOCKER_PUSH"
    docker_image_tag: "1.0"
    repository: "rpereira44/alt"
    integration_hash: "******************"
  - action: "Archive directory"
    type: "ZIP"
    local_path: "CA2/Part02/react-and-spring-data-rest-basic/build/libs"
    destination: "CA2/Part02/react-and-spring-data-rest-basic/build/libs.zip"
    deployment_excludes:
    - ".git"
````






#### Conclusion

In my opinion for a first time user, Buddy is more user-friendly, because he does a lot of mechanisms automatically. 
We only have to choose the action, and wright the information as simple command lines (Configuration / build configurations (example)- image).
After this first impression, I continue to think that in a long term, Jenkins is a better and more powerful tool for all the reasons referred in the conclusion between Jenkins and Bitbucket Pipeline.


